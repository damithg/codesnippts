﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AllAboutLinq.OB.Transaction;
using Newtonsoft.Json;
using NUnit.Framework;

namespace AllAboutLinq
{
    public class LinqBase
    {
        internal static string GetApplicationRoot()
        {
            var exePath = Path.GetDirectoryName(System.Reflection
                .Assembly.GetExecutingAssembly().CodeBase);
            Regex appPathMatcher = new Regex(@"(?<!fil)[A-Za-z]:\\+[\S\s]*?(?=\\+bin)");
            var appRoot = appPathMatcher.Match(exePath).Value;
            return appRoot;
        }

        internal static string GetJsonBlob()
        {
            // Get the path to json file
            var path = GetApplicationRoot();
            return path + @"./config/Santander-top-200-transactions.json";
        }
    }

    [TestFixture]
    public class LinqTests : LinqBase
    {
        [Test(Description = "Using JWT package")]
        public void GroupBy()
        {
            var file = GetJsonBlob();

            OBReadTransaction items;
            using (StreamReader r = new StreamReader(file))
            {
                string json = r.ReadToEnd();
                items = JsonConvert.DeserializeObject<OBReadTransaction>(json);
            }

            // Flattern the list of lists
            var flatternTransactions =
                items.Data.Transaction.GroupBy(row => row.BankTransactionCode)
                    .SelectMany(g => g.OrderBy(row => row.BankTransactionCode).Take(2))
                    .ToList();

            // Just get the list
            var nonFlatternTransactions = items.Data.Transaction.GroupBy(row => row.BankTransactionCode)
                .Select(g => g.OrderBy(row => row.BankTransactionCode).Take(2)).ToList();
        }
    }
}
