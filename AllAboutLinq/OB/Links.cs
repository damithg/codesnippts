﻿namespace AllAboutLinq.OB
{
    /// <summary>
    /// Links relevant to the payload
    /// </summary>
    public class Links
    {
        /// <summary>
        /// Gets or Sets Self
        /// </summary>
        public string Self { get; set; }

        /// <summary>
        /// Gets or Sets First
        /// </summary>
        public string First { get; set; }

        /// <summary>
        /// Gets or Sets Prev
        /// </summary>
        public string Prev { get; set; }

        /// <summary>
        /// Gets or Sets Next
        /// </summary>
        public string Next { get; set; }

        /// <summary>
        /// Gets or Sets Last
        /// </summary>
        public string Last { get; set; }
    }
}