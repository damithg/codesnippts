﻿using System;

namespace AllAboutLinq.OB
{
    /// <summary>
    /// Meta Data relevant to the payload
    /// </summary>
    public class Meta
    {
        /// <summary>
        /// Gets or Sets TotalPages
        /// </summary>
        public int? TotalPages { get; set; }

        /// <summary>
        /// Gets or Sets FirstAvailableDateTime
        /// </summary>
        public DateTimeOffset FirstAvailableDateTime { get; set; }

        /// <summary>
        /// Gets or Sets LastAvailableDateTime
        /// </summary>
        public DateTimeOffset LastAvailableDateTime { get; set; }
    }
}