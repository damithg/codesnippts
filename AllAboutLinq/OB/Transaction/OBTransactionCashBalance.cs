﻿namespace AllAboutLinq.OB.Transaction
{
    /// <summary>
    /// Set of elements used to define the balance as a numerical representation of the net increases and decreases in an account after a transaction entry is applied to the account.
    /// </summary>
    public class OBTransactionCashBalance
    {
        /// <summary>
        /// Gets or Sets CreditDebitIndicator
        /// </summary>
        public OBCreditDebit CreditDebitIndicator { get; set; }

        /// <summary>
        /// Gets or Sets Type
        /// </summary>
        public OBBalanceType? Type { get; set; }

        /// <summary>
        /// Gets or Sets Amount
        /// </summary>
        public OBDataAmount Amount { get; set; }
    }
}