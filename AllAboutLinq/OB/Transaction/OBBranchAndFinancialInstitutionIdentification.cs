﻿namespace AllAboutLinq.OB.Transaction
{
    /// <summary>
    /// Financial institution servicing an account for the creditor.
    /// </summary>
    public class OBBranchAndFinancialInstitutionIdentification
    {
        /// <summary>
        /// Gets or Sets SchemeName
        /// </summary>
        public string SchemeName { get; set; }

        /// <summary>
        /// Gets or Sets Identification
        /// </summary>
        public string Identification { get; set; }

        /// <summary>
        /// Gets or Sets Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or Sets PostalAddress
        /// </summary>
        public OBPostalAddress PostalAddress { get; set; }
    }
}