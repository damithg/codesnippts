﻿namespace AllAboutLinq.OB.Transaction
{
    /// <summary>
    /// Details of the merchant involved in the transaction.
    /// </summary>
    public class OBMerchantDetails
    {
        /// <summary>
        /// Name by which the merchant is known.
        /// </summary>
        public string MerchantName { get; set; }

        /// <summary>
        /// Category code conform to ISO 18245, related to the type of services or goods the merchant provides for the transaction.
        /// </summary>
        public string MerchantCategoryCode { get; set; }
    }
}