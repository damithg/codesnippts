﻿namespace AllAboutLinq.OB.Transaction
{
    public class OBReadTransaction
    {
        /// <summary>
        /// Gets or Sets Data
        /// </summary>
        public OBReadDataTransaction Data { get; set; }

        /// <summary>
        /// Gets or Sets Links
        /// </summary>
        public Links Links { get; set; }

        /// <summary>
        /// Gets or Sets Meta
        /// </summary>
        public Meta Meta { get; set; }
    }
}
