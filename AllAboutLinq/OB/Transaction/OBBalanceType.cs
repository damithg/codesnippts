﻿using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace AllAboutLinq.OB.Transaction
{
    /// <summary>
    /// Gets or Sets Type
    /// </summary>
    [JsonConverter(typeof(Newtonsoft.Json.Converters.StringEnumConverter))]
    public enum OBBalanceType
    {
        /// <summary>
        /// Enum ClosingAvailable for ClosingAvailable
        /// </summary>
        [EnumMember(Value = "ClosingAvailable")]
        ClosingAvailable = 1,

        /// <summary>
        /// Enum ClosingBooked for ClosingBooked
        /// </summary>
        [EnumMember(Value = "ClosingBooked")]
        ClosingBooked = 2,

        /// <summary>
        /// Enum ClosingCleared for ClosingCleared
        /// </summary>
        [EnumMember(Value = "ClosingCleared")]
        ClosingCleared = 3,

        /// <summary>
        /// Enum Expected for Expected
        /// </summary>
        [EnumMember(Value = "Expected")]
        Expected = 4,

        /// <summary>
        /// Enum ForwardAvailable for ForwardAvailable
        /// </summary>
        [EnumMember(Value = "ForwardAvailable")]
        ForwardAvailable = 5,

        /// <summary>
        /// Enum Information for Information
        /// </summary>
        [EnumMember(Value = "Information")]
        Information = 6,

        /// <summary>
        /// Enum InterimAvailable for InterimAvailable
        /// </summary>
        [EnumMember(Value = "InterimAvailable")]
        InterimAvailable = 7,

        /// <summary>
        /// Enum InterimBooked for InterimBooked
        /// </summary>
        [EnumMember(Value = "InterimBooked")]
        InterimBooked = 8,

        /// <summary>
        /// Enum InterimCleared for InterimCleared
        /// </summary>
        [EnumMember(Value = "InterimCleared")]
        InterimCleared = 9,

        /// <summary>
        /// Enum OpeningAvailable for OpeningAvailable
        /// </summary>
        [EnumMember(Value = "OpeningAvailable")]
        OpeningAvailable = 10,

        /// <summary>
        /// Enum OpeningBooked for OpeningBooked
        /// </summary>
        [EnumMember(Value = "OpeningBooked")]
        OpeningBooked = 11,

        /// <summary>
        /// Enum OpeningCleared for OpeningCleared
        /// </summary>
        [EnumMember(Value = "OpeningCleared")]
        OpeningCleared = 12,

        /// <summary>
        /// Enum PreviouslyClosedBooked for PreviouslyClosedBooked
        /// </summary>
        [EnumMember(Value = "PreviouslyClosedBooked")]
        PreviouslyClosedBooked = 13
    }
}