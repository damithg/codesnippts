﻿namespace AllAboutLinq.OB.Transaction
{
    /// <summary>
    /// Set of elements to describe the card instrument used in the transaction.
    /// </summary>
    public class OBTransactionCardInstrument
    {
        /// <summary>
        /// Name of the card scheme.
        /// </summary>
        public CardSchemeName? CardSchemeName { get; set; }

        /// <summary>
        /// The card authorisation type.
        /// </summary>
        public AuthorisationType? AuthorisationType { get; set; }

        /// <summary>
        /// Name of the cardholder using the card instrument.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Identification assigned by an institution to identify the card instrument used in the transaction. This identification is known by the account owner, and may be masked.
        /// </summary>
        public string Identification { get; set; }
    }
}