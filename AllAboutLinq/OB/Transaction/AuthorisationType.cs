﻿using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace AllAboutLinq.OB.Transaction
{
    /// <summary>
    /// The card authorisation type.
    /// </summary>
    [JsonConverter(typeof(Newtonsoft.Json.Converters.StringEnumConverter))]
    public enum AuthorisationType
    {
        /// <summary>
        /// Enum ConsumerDevice for ConsumerDevice
        /// </summary>
        [EnumMember(Value = "ConsumerDevice")]
        ConsumerDevice = 1,

        /// <summary>
        /// Enum Contactless for Contactless
        /// </summary>
        [EnumMember(Value = "Contactless")]
        Contactless = 2,

        /// <summary>
        /// Enum None for None
        /// </summary>
        [EnumMember(Value = "None")]
        None = 3,

        /// <summary>
        /// Enum PIN for PIN
        /// </summary>
        [EnumMember(Value = "PIN")]
        PIN = 4
    }
}