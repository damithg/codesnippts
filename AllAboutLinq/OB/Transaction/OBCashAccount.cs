﻿namespace AllAboutLinq.OB.Transaction
{
    /// <summary>
    /// Unambiguous identification of the account of the creditor, in the case of a debit transaction.
    /// </summary>
    public class OBCashAccount
    {
        /// <summary>
        /// Gets or Sets SchemeName
        /// </summary>
        public string SchemeName { get; set; }

        /// <summary>
        /// Gets or Sets Identification
        /// </summary>
        public string Identification { get; set; }

        /// <summary>
        /// Gets or Sets Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or Sets SecondaryIdentification
        /// </summary>
        public string SecondaryIdentification { get; set; }
    }
}