﻿using System;

namespace AllAboutLinq.OB.Transaction
{
    /// <summary>
    /// Set of elements used to provide details on the currency exchange.
    /// </summary>
    public class OBCurrencyExchange
    {
        /// <summary>
        /// Currency from which an amount is to be converted in a currency conversion.
        /// </summary>
        public string SourceCurrency { get; set; }

        /// <summary>
        /// Currency into which an amount is to be converted in a currency conversion.
        /// </summary>
        public string TargetCurrency { get; set; }

        /// <summary>
        /// Currency in which the rate of exchange is expressed in a currency exchange. In the example 1GBP &#x3D; xxxCUR, the unit currency is GBP.
        /// </summary>
        public string UnitCurrency { get; set; }

        /// <summary>
        /// Factor used to convert an amount from one currency into another. This reflects the price at which one currency was bought with another currency. Usage: ExchangeRate expresses the ratio between UnitCurrency and QuotedCurrency (ExchangeRate &#x3D; UnitCurrency/QuotedCurrency).
        /// </summary>
        public decimal? ExchangeRate { get; set; }

        /// <summary>
        /// Unique identification to unambiguously identify the foreign exchange contract.
        /// </summary>
        public string ContractIdentification { get; set; }

        /// <summary>
        /// Date and time at which an exchange rate is quoted.All dates in the JSON payloads are represented in ISO 8601 date-time format.  All date-time fields in responses must include the timezone. An example is below: 2017-04-05T10:43:07+00:00
        /// </summary>
        public DateTime? QuotationDate { get; set; }

        /// <summary>
        /// Gets or Sets InstructedAmount
        /// </summary>
        public OBDataAmount InstructedAmount { get; set; }
    }
}