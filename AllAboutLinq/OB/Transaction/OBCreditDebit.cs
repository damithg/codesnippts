﻿using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace AllAboutLinq.OB.Transaction
{
    /// <summary>
    /// Indicates whether the balance is a credit or a debit balance.  Usage: A zero balance is considered to be a credit balance.
    /// </summary>
    [JsonConverter(typeof(Newtonsoft.Json.Converters.StringEnumConverter))]
    public enum OBCreditDebit
    {
        /// <summary>
        /// Enum Credit for Credit
        /// </summary>
        [EnumMember(Value = "Credit")]
        CreditEnum = 1,

        /// <summary>
        /// Enum Debit for Debit
        /// </summary>
        [EnumMember(Value = "Debit")]
        DebitEnum = 2
    }
}