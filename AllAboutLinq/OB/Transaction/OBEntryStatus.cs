﻿using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace AllAboutLinq.OB.Transaction
{
    /// <summary>
    /// Gets or Sets Status
    /// </summary>
    [JsonConverter(typeof(Newtonsoft.Json.Converters.StringEnumConverter))]
    public enum OBEntryStatus
    {
        /// <summary>
        /// Enum Booked for Booked
        /// </summary>
        [EnumMember(Value = "Booked")]
        Booked = 1,

        /// <summary>
        /// Enum Pending for Pending
        /// </summary>
        [EnumMember(Value = "Pending")]
        Pending = 2
    }
}