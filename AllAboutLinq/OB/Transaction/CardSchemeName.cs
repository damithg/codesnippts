﻿using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace AllAboutLinq.OB.Transaction
{
    /// <summary>
    /// Name of the card scheme.
    /// </summary>
    [JsonConverter(typeof(Newtonsoft.Json.Converters.StringEnumConverter))]
    public enum CardSchemeName
    {
        /// <summary>
        /// Enum AmericanExpress for AmericanExpress
        /// </summary>
        [EnumMember(Value = "AmericanExpress")]
        AmericanExpress = 1,

        /// <summary>
        /// Enum Diners for Diners
        /// </summary>
        [EnumMember(Value = "Diners")]
        Diners = 2,

        /// <summary>
        /// Enum Discover for Discover
        /// </summary>
        [EnumMember(Value = "Discover")]
        Discover = 3,

        /// <summary>
        /// Enum MasterCard for MasterCard
        /// </summary>
        [EnumMember(Value = "MasterCard")]
        MasterCard = 4,

        /// <summary>
        /// Enum VISA for VISA
        /// </summary>
        [EnumMember(Value = "VISA")]
        VISA = 5
    }
}