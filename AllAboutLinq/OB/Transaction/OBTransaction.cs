﻿using System;
using System.Collections.Generic;

namespace AllAboutLinq.OB.Transaction
{
    /// <summary>
    /// Provides further details on an entry in the report.
    /// </summary>
    public class OBTransaction
    {
        /// <summary>
        /// Gets or Sets AccountId
        /// </summary>
        public string AccountId { get; set; }

        /// <summary>
        /// Gets or Sets TransactionId
        /// </summary>
        public string TransactionId { get; set; }

        /// <summary>
        /// Gets or Sets TransactionReference
        /// </summary>
        public string TransactionReference { get; set; }

        /// <summary>
        /// Gets or Sets StatementReference
        /// </summary>
        public List<string> StatementReference { get; set; }

        /// <summary>
        /// Gets or Sets CreditDebitIndicator
        /// </summary>
        public OBCreditDebit CreditDebitIndicator { get; set; }

        /// <summary>
        /// Gets or Sets Status
        /// </summary>
        public OBEntryStatus? Status { get; set; }

        /// <summary>
        /// Gets or Sets BookingDateTime
        /// </summary>
        public DateTimeOffset BookingDateTime { get; set; }

        /// <summary>
        /// Gets or Sets ValueDateTime
        /// </summary>
        public DateTimeOffset ValueDateTime { get; set; }

        /// <summary>
        /// Gets or Sets TransactionInformation
        /// </summary>
        public string TransactionInformation { get; set; }

        /// <summary>
        /// Gets or Sets AddressLine
        /// </summary>
        public string AddressLine { get; set; }

        /// <summary>
        /// Gets or Sets Amount
        /// </summary>
        public OBDataAmount Amount { get; set; }

        /// <summary>
        /// Gets or Sets ChargeAmount
        /// </summary>
        public OBDataAmount ChargeAmount { get; set; }

        /// <summary>
        /// Gets or Sets CurrencyExchange
        /// </summary>
        public OBCurrencyExchange CurrencyExchange { get; set; }

        /// <summary>
        /// Gets or Sets BankTransactionCode
        /// </summary>
        public OBBankTransactionCodeStructure BankTransactionCode { get; set; }

        /// <summary>
        /// Gets or Sets ProprietaryBankTransactionCode
        /// </summary>
        public ProprietaryBankTransactionCodeStructure ProprietaryBankTransactionCode { get; set; }

        /// <summary>
        /// Gets or Sets Balance
        /// </summary>
        public OBTransactionCashBalance Balance { get; set; }

        /// <summary>
        /// Gets or Sets MerchantDetails
        /// </summary>
        public OBMerchantDetails MerchantDetails { get; set; }

        /// <summary>
        /// Gets or Sets CreditorAgent
        /// </summary>
        public OBBranchAndFinancialInstitutionIdentification CreditorAgent { get; set; }

        /// <summary>
        /// Gets or Sets CreditorAccount
        /// </summary>
        public OBCashAccount CreditorAccount { get; set; }

        /// <summary>
        /// Gets or Sets DebtorAgent
        /// </summary>
        public OBBranchAndFinancialInstitutionIdentification DebtorAgent { get; set; }

        /// <summary>
        /// Gets or Sets DebtorAccount
        /// </summary>
        public OBCashAccount DebtorAccount { get; set; }

        /// <summary>
        /// Gets or Sets CardInstrument
        /// </summary>
        public OBTransactionCardInstrument CardInstrument { get; set; }

        /// <summary>
        /// Gets or Sets SupplementaryData
        /// </summary>
        public string SupplementaryData { get; set; }
    }
}