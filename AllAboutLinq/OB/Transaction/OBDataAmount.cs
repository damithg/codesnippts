﻿namespace AllAboutLinq.OB.Transaction
{
    /// <summary>
    /// Amount of money of the cash balance.
    /// </summary>
    public class OBDataAmount
    {
        /// <summary>
        /// Gets or Sets Amount
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Gets or Sets Currency
        /// </summary>
        public string Currency { get; set; }
    }
}