﻿using System.Collections.Generic;

namespace AllAboutLinq.OB.Transaction
{
    public class OBReadDataTransaction
    {
        /// <summary>
        /// Gets or Sets Transaction
        /// </summary>
        public List<OBTransaction> Transaction { get; set; }
    }
}