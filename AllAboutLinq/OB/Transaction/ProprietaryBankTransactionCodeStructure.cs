﻿namespace AllAboutLinq.OB.Transaction
{
    /// <summary>
    /// Set of elements to fully identify a proprietary bank transaction code.
    /// </summary>
    public class ProprietaryBankTransactionCodeStructure
    {
        /// <summary>
        /// Proprietary bank transaction code to identify the underlying transaction.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Identification of the issuer of the proprietary bank transaction code.
        /// </summary>
        public string Issuer { get; set; }
    }
}