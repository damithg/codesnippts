﻿namespace AllAboutLinq.OB.Transaction
{
    /// <summary>
    /// Set of elements used to fully identify the type of underlying transaction resulting in an entry.
    /// </summary>
    public class OBBankTransactionCodeStructure
    {
        /// <summary>
        /// Specifies the family within a domain.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Specifies the sub-product family within a specific family.
        /// </summary>
        public string SubCode { get; set; }
    }
}