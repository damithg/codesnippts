﻿using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace AllAboutLinq.OB.Transaction
{
    /// <summary>
    /// Gets or Sets AddressType
    /// </summary>
    [JsonConverter(typeof(Newtonsoft.Json.Converters.StringEnumConverter))]
    public enum OBAddressType
    {
        /// <summary>
        /// Enum Business for Business
        /// </summary>
        [EnumMember(Value = "Business")]
        Business = 1,

        /// <summary>
        /// Enum Correspondence for Correspondence
        /// </summary>
        [EnumMember(Value = "Correspondence")]
        Correspondence = 2,

        /// <summary>
        /// Enum DeliveryTo for DeliveryTo
        /// </summary>
        [EnumMember(Value = "DeliveryTo")]
        DeliveryTo = 3,

        /// <summary>
        /// Enum MailTo for MailTo
        /// </summary>
        [EnumMember(Value = "MailTo")]
        MailTo = 4,

        /// <summary>
        /// Enum POBox for POBox
        /// </summary>
        [EnumMember(Value = "POBox")]
        POBox = 5,

        /// <summary>
        /// Enum Postal for Postal
        /// </summary>
        [EnumMember(Value = "Postal")]
        Postal = 6,

        /// <summary>
        /// Enum Residential for Residential
        /// </summary>
        [EnumMember(Value = "Residential")]
        Residential = 7,

        /// <summary>
        /// Enum Statement for Statement
        /// </summary>
        [EnumMember(Value = "Statement")]
        Statement = 8
    }
}