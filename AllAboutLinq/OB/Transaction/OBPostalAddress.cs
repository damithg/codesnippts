﻿using System.Collections.Generic;

namespace AllAboutLinq.OB.Transaction
{
    /// <summary>
    /// Information that locates and identifies a specific address, as defined by postal services.
    /// </summary>
    public class OBPostalAddress
    {
        /// <summary>
        /// Gets or Sets AddressType
        /// </summary>
        public OBAddressType? AddressType { get; set; }

        /// <summary>
        /// Identification of a division of a large organisation or building.
        /// </summary>
        public string Department { get; set; }

        /// <summary>
        /// Identification of a sub-division of a large organisation or building.
        /// </summary>
        public string SubDepartment { get; set; }

        /// <summary>
        /// Gets or Sets StreetName
        /// </summary>
        public string StreetName { get; set; }

        /// <summary>
        /// Gets or Sets BuildingNumber
        /// </summary>
        public string BuildingNumber { get; set; }

        /// <summary>
        /// Gets or Sets PostCode
        /// </summary>
        public string PostCode { get; set; }

        /// <summary>
        /// Gets or Sets TownName
        /// </summary>
        public string TownName { get; set; }

        /// <summary>
        /// Identifies a subdivision of a country such as state, region, county.
        /// </summary>
        public string CountrySubDivision { get; set; }

        /// <summary>
        /// Nation with its own government.
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Gets or Sets AddressLine
        /// </summary>
        public List<string> AddressLine { get; set; }
    }
}