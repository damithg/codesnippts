﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace OOPrincipals
{
    [TestFixture]
    public class Class1
    {
    }

    public abstract class BaseGenerator
    {
        public void Get()
        {

        }
    }

    public class SpecificGenerator : BaseGenerator
    {

    }
}
