﻿using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;

namespace AllAboutJwt
{
    public class JwtIntegrationBase
    {
        internal static string GetApplicationRoot()
        {
            var exePath = Path.GetDirectoryName(System.Reflection
                .Assembly.GetExecutingAssembly().CodeBase);
            Regex appPathMatcher = new Regex(@"(?<!fil)[A-Za-z]:\\+[\S\s]*?(?=\\+bin)");
            var appRoot = appPathMatcher.Match(exePath).Value;
            return appRoot;
        }

        internal static X509Certificate2 GetX509Certificate()
        {
            // Get the certificate to sign the token
            var path = GetApplicationRoot();
            var certFilePath = path + @"./config/lHPb9e7MvYDUJGlb07XOtZ.pfx";
            return new X509Certificate2(certFilePath, "Gs82klb1");
        }
    }
}
