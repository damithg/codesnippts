﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using JWT.Algorithms;
using JWT.Builder;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using NUnit.Framework;

namespace AllAboutJwt
{
    [TestFixture]
    public class JwtIntegrationTests : JwtIntegrationBase
    {
        [Test(Description = "Using JWT package")]
        public void When_generating_signed_token_should_return_valid_token_v1()
        {
            // certificate
            var certificate = GetX509Certificate();

            var token = new JwtBuilder()
                .WithAlgorithm(new RS256Algorithm(certificate))
                .WithSecret("GQDstcKsx0NHjPOuXOYg5MbeJ1XT0uFiwDVvVBrk")
                .Issuer("4394bddd-8938-49a1-8e83-2787be772b8c")
                .Audience("https://modelobank2018.o3bank.co.uk:4201/token")
                .IssuedAt(DateTime.UtcNow)
                .AddClaim("exp", DateTimeOffset.UtcNow.AddHours(1).ToUnixTimeSeconds())
                .AddClaim("claim2", "claim2-value")
                .Build();

            Assert.IsNotNull(token);
        }

        [Test(Description = "Using Microsoft Identity")]
        public void When_generating_signed_token_should_return_valid_token_v2()
        {
            // certificate
            var certificate = GetX509Certificate();

            var descriptor = new SecurityTokenDescriptor();
            descriptor.SigningCredentials = new SigningCredentials(
                new X509SecurityKey(certificate), 
                "RS256");

            var claims = new List<Claim>();

            claims.Add(new Claim("kid", "8a919e66-6d34-4a26-ba58-ab6b4617d117"));
            claims.Add(new Claim("client_id", "8a919e66-6d34-4a26-ba58-ab6b4617d117"));
            claims.Add(new Claim("response_type", "code id_token"));
            claims.Add(new Claim("redirect_uri", "https://services.isg-gateway-test.com/api/OAuth/Confirm"));
            claims.Add(new Claim("state", "10d260bf-a7d9-444a-92d9-7b7a5f088208"));
            claims.Add(new Claim("nonce", "10d260bf-a7d9-444a-92d9-7b7a5f088208"));
            claims.Add(new Claim("scope", "openid accounts"));
            claims.Add(new Claim("aud", "https://as.aspsp.sandbox.lloydsbanking.com/oauth2"));
            claims.Add(new Claim("iss", "8a919e66-6d34-4a26-ba58-ab6b4617d117"));
            claims.Add(new Claim("jti", "8a919e66-6d34-4a26-ba58-ab6b4617d117"));
            claims.Add(new Claim("client_id", "8a919e66-6d34-4a26-ba58-ab6b4617d117"));
            claims.Add(new Claim("response_type", "code id_token"));
            claims.Add(new Claim("redirect_uri", "CLIENT_REDIRECT_URI"));
            claims.Add(new Claim("state", "10d260bf-a7d9-444a-92d9-7b7a5f088208"));
            claims.Add(new Claim("nonce", "10d260bf-a7d9-444a-92d9-7b7a5f088208"));
            claims.Add(new Claim("scope", "openid accounts"));
            claims.Add(new Claim("jti", "8a919e66-6d34-4a26-ba58-ab6b4617d117")); // CLIENT_ID

            var nbf = DateTime.UtcNow.AddSeconds(-1);
            var exp = DateTime.UtcNow.AddSeconds(120);
            var payload = new JwtPayload(
                "8a919e66-6d34-4a26-ba58-ab6b4617d117",
                "https://as.aspsp.sandbox.lloydsbanking.com/oauth2",
                claims,
                nbf,
                exp);

            var users = new Dictionary<string, object>();
            users.Add("actions", new List<string>() { "read", "create" });
            var scopes = new Dictionary<string, object>();
            scopes.Add("users", users);
            payload.Add("scopes", scopes);

            var jwtToken = new JwtSecurityToken(new JwtHeader(descriptor.SigningCredentials), payload);
            var jwtTokenHandler = new JwtSecurityTokenHandler();
            var jwt = jwtTokenHandler.WriteToken(jwtToken);

            Assert.NotNull(jwt);
        }

        // TODO : testing the jwt token issue
        private static string GetAccessToken()
        {
            var path = GetApplicationRoot();

            string certFilePath = path + @"./config/transport-YIy7ESMR7Ggu6TgWmnb1pY.pfx";

            // certificate
            var certificate = new X509Certificate2(certFilePath, "Gs82klb1");

            // header
            var header = new { typ = "JWT", alg = "RS256" };

            // claimset
            var times = GetExpiryAndIssueDate();
            var claimset = new
            {
                iss = "",
                scope = "accounts",
                aud = "https://modelobank2018.o3bank.co.uk:4201/token",
                iat = times[0],
                exp = times[1],
            };

            // encoded header
            var headerSerialized = JsonConvert.SerializeObject(header);
            var headerBytes = Encoding.UTF8.GetBytes(headerSerialized);
            var headerEncoded = Convert.ToBase64String(headerBytes);

            // encoded claimset
            var claimsetSerialized = JsonConvert.SerializeObject(claimset);
            var claimsetBytes = Encoding.UTF8.GetBytes(claimsetSerialized);
            var claimsetEncoded = Convert.ToBase64String(claimsetBytes);

            // input
            var input = headerEncoded + "." + claimsetEncoded;
            var inputBytes = Encoding.UTF8.GetBytes(input);

            // signiture
            var rsa = certificate.PrivateKey as RSACryptoServiceProvider;
            var cspParam = new CspParameters
            {
                KeyContainerName = rsa.CspKeyContainerInfo.KeyContainerName,
                KeyNumber = rsa.CspKeyContainerInfo.KeyNumber == KeyNumber.Exchange ? 1 : 2
            };

            var aescsp = new RSACryptoServiceProvider(cspParam) { PersistKeyInCsp = false };
            var signatureBytes = aescsp.SignData(inputBytes, "SHA256");
            var signatureEncoded = Convert.ToBase64String(signatureBytes);

            // jwt
            var jwt = headerEncoded + "." + claimsetEncoded + "." + signatureEncoded;

            var content = new NameValueCollection();

            content["assertion"] = jwt;
            content["grant_type"] = "urn:ietf:params:oauth:grant-type:jwt-bearer";

            //string response = Encoding.UTF8.GetString(client.UploadValues(uri, "POST", content));

            //var result = JsonConvert.DeserializeObject<dynamic>(response);

            return jwt;
        }

        private static int[] GetExpiryAndIssueDate()
        {
            var utc0 = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            var issueTime = DateTime.UtcNow;

            var iat = (int)issueTime.Subtract(utc0).TotalSeconds;
            var exp = (int)issueTime.AddMinutes(55).Subtract(utc0).TotalSeconds;

            return new[] { iat, exp };
        }
    }
}